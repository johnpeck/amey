// Creates CUI SJ5-43502PM panel mount 1/8-inch audio jack

function mm_to_in (mm) = mm/25.4;

module headphone() {
     shaft_height = mm_to_in(10.2);
     shaft_diameter = mm_to_in(8.0);

     flange_height = mm_to_in(1.5);
     flange_diameter = mm_to_in(9.0);

     thread_height = mm_to_in(4.5);
     thread_diameter = mm_to_in(7.0);

     jack_diameter = mm_to_in(3.6);


     // Shaft
     color( "SlateGray") {
	  translate([0, 0, 0 - shaft_height - flange_height]) {
	       cylinder( h = shaft_height, r = shaft_diameter/2, $fn=100);	  
	  }	  
     }
     // Flange
     color( "LightGray" ) {
	  translate([0, 0, 0 - flange_height]) {
	       cylinder( h = flange_height, r = flange_diameter/2, $fn=100);
	  }	  
     }
     // Threads
     difference() {
	  color( "LightGray" ) {
	       cylinder( h = thread_height, r = thread_diameter/2, $fn=100);
	  }
	  color( "Black" ) {
	       cylinder( h = 2 * thread_height, r = jack_diameter/2, $fn=100);
	  }
     }
    


}


headphone();



    
