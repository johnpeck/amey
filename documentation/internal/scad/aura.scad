
function mm_to_in (mm) = mm/25.4;

module aura() {
     // Create the Aura Sound NS3-193-8A driver
     magnet_height = mm_to_in(25.1);
     magnet_diameter = mm_to_in(36.0);

     // Distance from the bottom of the magnet to the flange
     magnet_to_flange = mm_to_in(54.9);

     basket_diameter = mm_to_in(75.5);

     // Length of the square in which the flange is inscribed
     flange_size = mm_to_in(88.5);

     // Guess at flange thickness
     flange_thickness = 0.05;

     bolt_circle_diameter = mm_to_in(93.0);

     bolt_hole_diameter = mm_to_in(4.3);

     // Diameter of the glue ring around the surround
     surround_glue_diameter = 3.3;

     // Magnet
     color( "SlateGray") {
	  translate([0, 0, 0 - magnet_to_flange]) {
	       cylinder( h = magnet_height, r = magnet_diameter/2, $fn=100);	  
	  }	  
     }
     // Basket
     color( "LightGray" ) {
	  translate([0, 0, 0 - magnet_to_flange + magnet_height]) {
	       cylinder( h = magnet_to_flange - magnet_height,
			 r = basket_diameter/2, $fn=100);
	  }	  
     }

     // Flange
     translate([0, 0, 0]) {
	  difference(){
	       color( "gray" ) {
		    translate([-flange_size/2, -flange_size/2, 0]) {
			 cube([flange_size, flange_size, flange_thickness]);
		    }
	       }
	       union() {
		    translate([bolt_circle_diameter/(2 * 1.414),
			       bolt_circle_diameter/(2 * 1.414),
			       -flange_thickness/2]) {
			 cylinder ( h = 2 * flange_thickness,
				    r = bolt_hole_diameter/2,
				    $fn=100);	  
		    }
		    translate([-bolt_circle_diameter/(2 * 1.414),
			       bolt_circle_diameter/(2 * 1.414),
			       -flange_thickness/2]) {
			 cylinder ( h = 2 * flange_thickness,
				    r = bolt_hole_diameter/2,
				    $fn=100);	  
		    }
		    translate([-bolt_circle_diameter/(2 * 1.414),
			       -bolt_circle_diameter/(2 * 1.414),
			       -flange_thickness/2]) {
			 cylinder ( h = 2 * flange_thickness,
				    r = bolt_hole_diameter/2,
				    $fn=100);	  
		    }
		    translate([bolt_circle_diameter/(2 * 1.414),
			       -bolt_circle_diameter/(2 * 1.414),
			       -flange_thickness/2]) {
			 cylinder ( h = 2 * flange_thickness,
				    r = bolt_hole_diameter/2,
				    $fn=100);	  
		    }
	       
	       }
	  }	  
     }
     // Surround
     translate([0, 0, flange_thickness]) {
	  color( "DarkSlateGray") {
	       cylinder ( h = flange_thickness,
		    r = surround_glue_diameter / 2,
		    $fn=100);
	  }
     }
   
}


aura();



    
