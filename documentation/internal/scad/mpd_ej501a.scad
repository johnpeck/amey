// Creates MPD EJ501A 2.1 x 5.5mm DC barrel jack

function mm_to_in (mm) = mm/25.4;

module barrel() {

     flange_height = mm_to_in(1.5);
     flange_diameter = mm_to_in(10.8);

     shaft_height = mm_to_in(10.2 - flange_height);
     shaft_diameter = mm_to_in(8.0);

     thread_height = mm_to_in(5.5);
     thread_diameter = mm_to_in(8.0);

     jack_diameter = mm_to_in(5.6);


     // Shaft
     color( "SlateGray") {
	  translate([0, 0, 0 - shaft_height - flange_height]) {
	       cylinder( h = shaft_height, r = shaft_diameter/2, $fn=100);	  
	  }	  
     }
     // Flange
     color( "LightGray" ) {
	  translate([0, 0, 0 - flange_height]) {
	       cylinder( h = flange_height, r = flange_diameter/2, $fn=100);
	  }	  
     }
     // Threads
     difference() {
	  color( "LightGray" ) {
	       cylinder( h = thread_height, r = thread_diameter/2, $fn=100);
	  }
	  color( "Black" ) {
	       cylinder( h = 2 * thread_height, r = jack_diameter/2, $fn=100);
	  }
     }
    


}


barrel();



    
