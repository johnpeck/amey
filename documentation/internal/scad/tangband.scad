
function mm_to_in (mm) = mm/25.4;

module tangband() {
     // Create the Tang Band W1-1070SH driver

     // Magnet thickness
     magnet_thickness = mm_to_in(10);

     // Magnet diameter
     magnet_diameter = mm_to_in(31.3);

     // Distance from the bottom of the magnet to the flange.  The
     // basket thickness is calculated.
     magnet_to_flange = mm_to_in(17.6);

     basket_diameter = mm_to_in(40.0);

     // Length of the square in which the flange is inscribed
     flange_size = mm_to_in(43.0);

     // Flange thickness
     flange_thickness = mm_to_in(2.5);

     bolt_circle_diameter = mm_to_in(48.0);

     bolt_hole_diameter = mm_to_in(3.4);

     // Diameter of the glue ring around the surround
     surround_glue_diameter = 1.5;

     // How far the surround extends beyond the flange
     surround_height = mm_to_in(1.5);

     // Magnet
     color( "SlateGray") {
	  translate([0, 0, 0 - magnet_to_flange]) {
	       cylinder( h = magnet_thickness, r = magnet_diameter/2, $fn=100);	  
	  }	  
     }
     // Basket
     color( "LightGray" ) {
	  translate([0, 0, 0 - magnet_to_flange + magnet_thickness]) {
	       cylinder( h = magnet_to_flange - magnet_thickness,
			 r = basket_diameter/2, $fn=100);
	  }	  
     }

     // Flange
     translate([0, 0, 0]) {
	  difference(){
	       color( "gray" ) {
		    translate([-flange_size/2, -flange_size/2, 0]) {
			 cube([flange_size, flange_size, flange_thickness]);
		    }
	       }
	       union() {
		    translate([bolt_circle_diameter/(2 * 1.414),
			       bolt_circle_diameter/(2 * 1.414),
			       -flange_thickness/2]) {
			 cylinder ( h = 2 * flange_thickness,
				    r = bolt_hole_diameter/2,
				    $fn=100);	  
		    }
		    translate([-bolt_circle_diameter/(2 * 1.414),
			       bolt_circle_diameter/(2 * 1.414),
			       -flange_thickness/2]) {
			 cylinder ( h = 2 * flange_thickness,
				    r = bolt_hole_diameter/2,
				    $fn=100);	  
		    }
		    translate([-bolt_circle_diameter/(2 * 1.414),
			       -bolt_circle_diameter/(2 * 1.414),
			       -flange_thickness/2]) {
			 cylinder ( h = 2 * flange_thickness,
				    r = bolt_hole_diameter/2,
				    $fn=100);	  
		    }
		    translate([bolt_circle_diameter/(2 * 1.414),
			       -bolt_circle_diameter/(2 * 1.414),
			       -flange_thickness/2]) {
			 cylinder ( h = 2 * flange_thickness,
				    r = bolt_hole_diameter/2,
				    $fn=100);	  
		    }
	       
	       }
	  }	  
     }
     // Surround
     translate([0, 0, flange_thickness]) {
	  color( "DarkSlateGray") {
	       cylinder ( h = surround_height,
		    r = surround_glue_diameter / 2,
		    $fn=100);
	  }
     }
   
}


tangband();



    
