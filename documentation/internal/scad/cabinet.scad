
function mm_to_in (mm) = mm/25.4;



//************************* View settings **************************//

// Gap length scaler
//
// Set this to zero to make the actual drawing.
gap_scaler = 1;

// Show PCB baffle in drawing
show_pcb = true;

// Show drivers in drawing
show_drivers = true;

// Show nuts and bolts in drawing
show_bolts = true;

// Show abs frame in drawing
show_abs = true;

// Show plywood in drawing
show_plywood = true;

// Show rear connector panel
show_al = true;

// Gap between pieces of plywood for illustration purposes.  Set to
// zero to get actual dimensions.
plywood_gap = 0.05 * gap_scaler;

// Gap between the cabinet front face and the pcb for illustration
// purposes.  Set to zero to get actual dimensions.
pcb_gap = 1 * gap_scaler;

// Gap between the cabinet front face and the ABS sheet for
// illustration purposes. Set to zero to get actual dimensions.
abs_gap = 1 * gap_scaler;

// Gap between the rear panel plywood and the aluminum connector plate
al_gap = 1 * gap_scaler;

// Gap betwen bolts and their first surface
bolt_gap = 0.1 * gap_scaler;

pem_gap = 0.1 * gap_scaler;

//**************************** Plywood *****************************//

// Internal dimensions (inches)
internal_dimensions = [8, 5, 5];

// Plywood thickness
plywood_thickness = 0.5;

// Diameter of the hole in the back of the box (inches)
plywood_hole_diameter = 2;

// Polygons need to overlap to create a solid object
plywood_antigap = 0.001;


//*************************** ABS sheet ****************************//

// Common ABS thickness is 1/8 inch.  Front Panel Designer only allows
// integral mm thicknesses, so 3mm = 0.118 inches.
abs_thickness = 0.118;

// Window in ABS sheet
abs_window_dimensions = [8, 4];

// Use 1/16-inch thick PCB
pcb_thickness = 0.063;

//************************* Aluminum panel *************************//

// Rear connector panel thickness (inches)
al_thickness = 0.098;

al_dimensions = [3, 3];

//***************************** Woofer *****************************//

use <aura.scad>;

// Woofer diameter
//
// AuraSound NS3-193-8A cutout diameter is 75.5mm
woofer_diameter = mm_to_in(75.5);

// Gap between PCB and woofer flange for illustration purposes.  Set
// to zero to get actual dimensions.
woofer_gap = 0.3 * gap_scaler;

// Mounting hole diameter for woofer
woofer_mounting_hole_diameter = mm_to_in(4.3);

// Woofer bolt circle
woofer_bolt_circle_diameter = mm_to_in(93.0);

//**************************** Tweeter *****************************//

use <tangband.scad>;

// Tweeter diameter
//
// Tang-Band W1-1070SH cutout diameter is 43.5mm
tweeter_diameter = mm_to_in(43.5);

tweeter_mounting_hole_diameter = mm_to_in(3.4);

tweeter_bolt_circle_diameter = mm_to_in(48.0);

tweeter_flange_thickness = mm_to_in(2.5);

root2 = 1.414;

//*************************** Inner PCB ****************************//

inpcb_dimensions = [6, 4];

inpcb_thickness = 0.064;

// Spacer (standoff) from bottom plywood
inpcb_spacer = 0.25;

use <bolts.scad>;
use <mpd_ej501a.scad>;
use <cui_sj5-43502pm.scad>;

module pcb( size, label = false ) {
     // Create a PCB
     //
     // Arguments:
     //   size -- [x dimension (inches), y dimension (inches), thickness (inches)]
     //   label -- Descriptive text
     color("green") {
	  translate([size[0]/2, size[1]/2, 0]) {
	       cube( [size[0], size[1], pcb_thickness], center = true );
	  }

     }
     echo( str("PCB is ", size[0], " x ", size[1], " inches, ", pcb_thickness, " thick."));
}

module pcb_cutout_round( location, radius ) {
     // Create a round hole in the PCB
     //
     // Arguments:
     //   location -- [x position (inches), y position (inches)]
     //   radius -- Radius of the hole (inches)
     color("DarkGreen") {
	  translate([location[0], location[1], 0]) {
	       cylinder(h = pcb_thickness * 2, r = radius, center = true, $fn = 100);
	  }
     }
     echo ( str("Round hole in PCB at (", location[0], ", ", location[1],
		") inches, radius of ", radius));
}

module plywood( width, length, label = false ) {
     // Create a plywood piece
     //
     // Arguments:
     //   width -- X dimension
     //   length -- Y dimension
     //   label -- Descriptive text
     color("BurlyWood") {
	  translate([width/2, length/2, 0]) {
	       cube( [width, length, plywood_thickness], center = true );
	  }
     }
     echo( str("Plywood piece: ",
	       width, " x ",
	       length, " inches, ",
	       plywood_thickness, " thick."));
}

module plywood_cutout( width, length, label = false ) {
     // Create a cutout in a plywood piece
     //
     // Arguments:
     //   width -- X dimension
     //   length -- Y dimension
     //   label -- Descriptive text
     color("BurlyWood") {
	  translate([0,0,-0.05]) {
	       cube( [width, length, plywood_thickness + 0.1] );
	  }
     }
}

module plywood_cutout_round( location, radius ) {
     // Create a round hole in the plywood
     //
     // Arguments:
     //   location -- [x position (inches), y position (inches)]
     //   radius -- Radius of the hole (inches)
     color("Black") {
	  translate([location[0], location[1], 0]) {
	       cylinder(h = plywood_thickness * 2, r = radius, center = true, $fn = 100);
	  }
     }
     echo ( str("Round hole in plywood at (", location[0], ", ", location[1],
		") inches, radius of ", radius));
}

module hardwood( length, label = false ) {
     color("Sienna") {
	  cube( [hardwood_dimensions[0], hardwood_dimensions[1], length]);
     }
}

module abs_sheet( width, length, label = false ) {
     // Create an ABS sheet
     //
     // Arguments:
     //   width -- X dimension
     //   length -- Y dimension
     //   label -- Descriptive text
     color("Gray") {
	  translate([width/2, length/2, 0]) {
	       cube( [width, length, abs_thickness], center = true);
	  }

     }
     echo( str("ABS frame is ",
	       width, " x ",
	       length, " inches, ",
	       abs_thickness, " thick."));
}

module al_sheet( width, length, label = false ) {
     // Create an aluminum sheet
     //
     // Arguments:
     //   width -- X dimension
     //   length -- Y dimension
     //   label -- Descriptive text
     color("Gray") {
	  translate([width/2, length/2, 0]) {
	       cube( [width, length, al_thickness], center = true);
	  }

     }
     echo( str("Aluminum panel is ",
	       width, " x ",
	       length, " inches, ",
	       al_thickness, " thick."));
}

module abs_cutout_rectangle( location, size , label = false ) {
     // Create an ABS sheet cutout.
     //
     // Arguments:
     //   location -- [x position (inches), y position (inches)]
     //   size -- [x dimension, y dimension]
     //   label -- Descriptive text
     color("Gray") {
	  translate([location[0], location[1], 0]) {
	       cube( [size[0], size[1], 2 * abs_thickness], center = true);
	  }
     }
     window_corners = [[location[0] + size[0]/2, location[1] - size[1]/2],
		       [location[0] + size[0]/2, location[1] + size[1]/2],
		       [location[0] - size[0]/2, location[1] + size[1]/2],
		       [location[0] - size[0]/2, location[1] - size[1]/2]];

     echo( str("ABS window corners at ",
	       "(", window_corners[0][0], ",", window_corners[0][1], "), ",
	       "(", window_corners[1][0], ",", window_corners[1][1], "), ",
	       "(", window_corners[2][0], ",", window_corners[2][1], "), ",
	       "(", window_corners[3][0], ",", window_corners[3][1], ")"));
     echo( str("ABS window measures ", size[0], "x", size[1]));
}

module abs_cutout_round( location, radius ) {
     // Create a round hole in the ABS sheet
     //
     // Arguments:
     //   location -- [x position (inches), y position (inches)]
     //   radius -- Radius of the hole (inches)
     color("Black") {
	  translate([location[0], location[1], 0]) {
	       cylinder(h = abs_thickness * 2, r = radius, center = true, $fn = 100);
	  }
     }
     echo ( str("Round hole in ABS at (", location[0], ", ", location[1],
		") inches, radius of ", radius));
}

module al_cutout_round( location, radius ) {
     // Create a round hole in the aluminum connector panel
     //
     // Arguments:
     //   location -- [x position (inches), y position (inches)]
     //   radius -- Radius of the hole (inches)
     color("Black") {
	  translate([al_dimensions[0] - location[0],
		     location[1], 0]) {
	       cylinder(h = al_thickness * 2, r = radius, center = true, $fn = 100);
	  }
     }
     echo ( str("Round hole in connector panel at (", location[0], ", ", location[1],
		") inches, radius of ", radius));
}





//************************* Draw front PCB *************************//
external_dimensions = [internal_dimensions[0] + 2 * plywood_thickness,
     internal_dimensions[1] + abs_thickness + pcb_thickness,
     internal_dimensions[2] + 2 * plywood_thickness];

pcb_dimensions = [internal_dimensions[0] + 2 * plywood_thickness,
		  internal_dimensions[2] + 2 * plywood_thickness + 2 * plywood_gap];

tweeter_centers = [[(pcb_dimensions[0] - woofer_diameter)/4,
		    pcb_dimensions[1]/2],
		   [(3 * pcb_dimensions[0] + woofer_diameter)/4,
		    pcb_dimensions[1]/2]];

pcb_mounting_holes = [
     [plywood_thickness/2, plywood_thickness/2],
     [pcb_dimensions[0]/2, plywood_thickness/2],
     [pcb_dimensions[0]-plywood_thickness/2, plywood_thickness/2],
     [plywood_thickness/2, pcb_dimensions[1]/2],
     [pcb_dimensions[0] - plywood_thickness/2, pcb_dimensions[1]/2],
     [plywood_thickness/2, pcb_dimensions[1] - plywood_thickness/2],
     [pcb_dimensions[0]/2, pcb_dimensions[1] - plywood_thickness/2],
     [pcb_dimensions[0] - plywood_thickness/2,
      pcb_dimensions[1] - plywood_thickness/2]];

woofer_mounting_holes = [
     [pcb_dimensions[0]/2 + woofer_bolt_circle_diameter/(2 * root2),
      pcb_dimensions[1]/2 + woofer_bolt_circle_diameter/(2 * root2)],
     [pcb_dimensions[0]/2 - woofer_bolt_circle_diameter/(2 * root2),
      pcb_dimensions[1]/2 + woofer_bolt_circle_diameter/(2 * root2)],
      [pcb_dimensions[0]/2 - woofer_bolt_circle_diameter/(2 * root2),
       pcb_dimensions[1]/2 - woofer_bolt_circle_diameter/(2 * root2)],
     [pcb_dimensions[0]/2 + woofer_bolt_circle_diameter/(2 * root2),
      pcb_dimensions[1]/2 - woofer_bolt_circle_diameter/(2 * root2)]
     ];

tweeter_mounting_holes = [
     [
	  [tweeter_centers[0][0] + tweeter_bolt_circle_diameter/(2 * root2),
	   tweeter_centers[0][1] + tweeter_bolt_circle_diameter/(2 * root2)],
	  [tweeter_centers[0][0] - tweeter_bolt_circle_diameter/(2 * root2),
	   tweeter_centers[0][1] + tweeter_bolt_circle_diameter / (2 * root2)],
	  [tweeter_centers[0][0] - tweeter_bolt_circle_diameter/(2 * root2),
	   tweeter_centers[0][1] - tweeter_bolt_circle_diameter / (2 * root2)],
	  [tweeter_centers[0][0] + tweeter_bolt_circle_diameter/(2 * root2),
	   tweeter_centers[0][1] - tweeter_bolt_circle_diameter / (2 * root2)]
	  ],
     [
	  [tweeter_centers[1][0] + tweeter_bolt_circle_diameter/(2 * root2),
	   tweeter_centers[1][1] + tweeter_bolt_circle_diameter/(2 * root2)],
	  [tweeter_centers[1][0] - tweeter_bolt_circle_diameter/(2 * root2),
	   tweeter_centers[1][1] + tweeter_bolt_circle_diameter / (2 * root2)],
	  [tweeter_centers[1][0] - tweeter_bolt_circle_diameter/(2 * root2),
	   tweeter_centers[1][1] - tweeter_bolt_circle_diameter / (2 * root2)],
	  [tweeter_centers[1][0] + tweeter_bolt_circle_diameter/(2 * root2),
	   tweeter_centers[1][1] - tweeter_bolt_circle_diameter / (2 * root2)]
	  ]];

woofer_center = [pcb_dimensions[0]/2, pcb_dimensions[1]/2];
if (show_pcb) {
     translate([0, 0 - abs_gap - abs_thickness - pcb_gap - pcb_thickness/2, 0]) {
	  rotate([90,0,0]){
	       difference() {
		    pcb( [pcb_dimensions[0],
			  pcb_dimensions[1],
			  pcb_thickness], "Front baffle" );
		    union() {
			 // AuraSound NS3-193-8A cutout diameter is 75.5mm
			 pcb_cutout_round( [pcb_dimensions[0]/2, pcb_dimensions[1]/2],
					   woofer_diameter / 2);
			 // Mounting holes for woofer.  I'll use the same
			 // PEMs as for PCB mounting, so I'll use 0.213 inch diameter
			 for (location = woofer_mounting_holes) {
			      pcb_cutout_round( location,
						0.213 / 2);
			 }

			 // Tang-Band W1-1070SH cutout diameter is 43.5mm
			 pcb_cutout_round( tweeter_centers[0], tweeter_diameter / 2);
			 // Mounting holes for tweeter #1
			 pcb_cutout_round( [tweeter_centers[0][0] +
					    tweeter_bolt_circle_diameter/(2 * root2),
					    tweeter_centers[0][1] +
					    tweeter_bolt_circle_diameter / (2 * root2)],
					   tweeter_mounting_hole_diameter / 2);
			 pcb_cutout_round( [tweeter_centers[0][0] -
					    tweeter_bolt_circle_diameter/(2 * root2),
					    tweeter_centers[0][1] +
					    tweeter_bolt_circle_diameter / (2 * root2)],
					   tweeter_mounting_hole_diameter / 2);
			 pcb_cutout_round( [tweeter_centers[0][0] -
					    tweeter_bolt_circle_diameter/(2 * root2),
					    tweeter_centers[0][1] -
					    tweeter_bolt_circle_diameter / (2 * root2)],
					   tweeter_mounting_hole_diameter / 2);
			 pcb_cutout_round( [tweeter_centers[0][0] +
					    tweeter_bolt_circle_diameter/(2 * root2),
					    tweeter_centers[0][1] -
					    tweeter_bolt_circle_diameter / (2 * root2)],
					   tweeter_mounting_hole_diameter / 2);
			 // Tweeter #2
			 pcb_cutout_round( tweeter_centers[1], tweeter_diameter / 2);
			 // Mounting holes for tweeter #2
			 pcb_cutout_round( [tweeter_centers[1][0] +
					    tweeter_bolt_circle_diameter/(2 * root2),
					    tweeter_centers[1][1] +
					    tweeter_bolt_circle_diameter / (2 * root2)],
					   tweeter_mounting_hole_diameter / 2);
			 pcb_cutout_round( [tweeter_centers[1][0] -
					    tweeter_bolt_circle_diameter/(2 * root2),
					    tweeter_centers[1][1] +
					    tweeter_bolt_circle_diameter / (2 * root2)],
					   tweeter_mounting_hole_diameter / 2);
			 pcb_cutout_round( [tweeter_centers[1][0] -
					    tweeter_bolt_circle_diameter/(2 * root2),
					    tweeter_centers[1][1] -
					    tweeter_bolt_circle_diameter / (2 * root2)],
					   tweeter_mounting_hole_diameter / 2);
			 pcb_cutout_round( [tweeter_centers[1][0] +
					    tweeter_bolt_circle_diameter/(2 * root2),
					    tweeter_centers[1][1] -
					    tweeter_bolt_circle_diameter / (2 * root2)],
					   tweeter_mounting_hole_diameter / 2);
			 // Mounting holes for ABS sheet.  Free-fit holes
			 // for #6 are 0.150 in diameter.
			 pcb_cutout_round( [plywood_thickness/2, plywood_thickness/2],
					   0.150/2);
			 pcb_cutout_round( [pcb_dimensions[0]/2, plywood_thickness/2],
					   0.150/2);
			 pcb_cutout_round( [pcb_dimensions[0]-plywood_thickness/2,
					    plywood_thickness/2],
					   0.150/2);
			 pcb_cutout_round( [plywood_thickness/2, pcb_dimensions[1]/2],
					   0.150/2);
			 pcb_cutout_round( [pcb_dimensions[0] - plywood_thickness/2,
					    pcb_dimensions[1]/2],
					   0.150/2);
			 pcb_cutout_round( [plywood_thickness/2,
					    pcb_dimensions[1] - plywood_thickness/2],
					   0.150/2);
			 pcb_cutout_round( [pcb_dimensions[0]/2,
					    pcb_dimensions[1] - plywood_thickness/2],
					   0.150/2);
			 pcb_cutout_round( [pcb_dimensions[0] - plywood_thickness/2,
					    pcb_dimensions[1] - plywood_thickness/2],
					   0.150/2);
		    }

	       }

	  }
     }
}

// Mounting bolts for tweeters
if (show_bolts) {
     translate([0, 0 - abs_gap - abs_thickness - pcb_gap -
		pcb_thickness - woofer_gap - tweeter_flange_thickness - bolt_gap, 0]) {
	  for (location = tweeter_mounting_holes[0]) {
	       translate([location[0], 0, location[1]]) {
		    rotate([90,0,0]){
			 socket_cap_four(5/16);
		    }
	       }
	  }
	  for (location = tweeter_mounting_holes[1]) {
	       translate([location[0], 0, location[1]]) {
		    rotate([90,0,0]){
			 socket_cap_four(5/16);
		    }
	       }
	  }
     }
}

// Mounting bolts for woofer
if (show_bolts) {
     translate([0, 0 - abs_gap - abs_thickness - pcb_gap -
		pcb_thickness - woofer_gap - bolt_gap, 0]) {
	  for (location = woofer_mounting_holes) {
	       translate([location[0], 0, location[1]]) {
		    rotate([90,0,0]){
			 socket_cap_six(5/16);
		    }
	       }
	  }
     }
}

// PEMs in PCB for mounting woofer
if (show_bolts) {
     translate([0, 0 - abs_gap - pcb_gap + pem_gap, 0]) {
	  for (location = woofer_mounting_holes) {
	       translate([location[0], 0, location[1]]) {
		    rotate([270,0,0]){
			 pem_six();
		    }
	       }
	  }
     }
}

// PEMs in PCB for mounting tweeter
if (show_bolts) {
     translate([0, 0 - abs_gap - pcb_gap + pem_gap, 0]) {
	  for (location = tweeter_mounting_holes[0]) {
	       translate([location[0], 0, location[1]]) {
		    rotate([270,0,0]){
			 pem_four();
		    }
	       }
	  }
	  for (location = tweeter_mounting_holes[1]) {
	       translate([location[0], 0, location[1]]) {
		    rotate([270,0,0]){
			 pem_four();
		    }
	       }
	  } 
     }
}

// Mounting bolts for front PCB
if (show_bolts) {
     translate([0, 0 - abs_gap - abs_thickness - pcb_gap - pcb_thickness - bolt_gap, 0]) {
	  for (location = pcb_mounting_holes) {
	       translate([location[0], 0, location[1]]) {
		    rotate([90,0,0]){
			 socket_cap_six(5/16);
		    }
	       }
	  }
     }
}

// PEMs in ABS for mounting PCB
if (show_bolts) {
     translate([0, 0 - abs_gap + pem_gap, 0]) {
	  for (location = pcb_mounting_holes) {
	       translate([location[0], 0, location[1]]) {
		    rotate([270,0,0]){
			 pem_six();
		    }
	       }
	  }
     }
}

// Woofer
if (show_drivers) {
	  translate([0,
		     0 - abs_gap - abs_thickness - pcb_gap - pcb_thickness - woofer_gap,
		     0]) {
	       translate([woofer_center[0], 0, woofer_center[1]]) {
		    rotate([90,0,0]){
			 aura();

		    }
	       }
     }
}

// Tweeters
if (show_drivers) {
     translate([tweeter_centers[0][0],
		0 - abs_thickness - pcb_gap - abs_gap - pcb_thickness - woofer_gap,
		pcb_dimensions[1]/2]) {
	  rotate([90,0,0]){
	       tangband();
	  }
     }
     translate([tweeter_centers[1][0],
		0 - abs_thickness - pcb_gap - abs_gap - pcb_thickness - woofer_gap,
		pcb_dimensions[1]/2]) {
	  rotate([90,0,0]){
	       tangband();
	  }
     }
}

// Front ABS
//
// Layer y-positions are specified using the layer's center.  This
// simplifies making holes, since you can specify the layer and hole
// centers to be the same.
if (show_abs) {
     translate([0, 0 - abs_gap - abs_thickness/2, 0]) {
	  difference() {
	       rotate([90,0,0]){
		    abs_sheet( internal_dimensions[0] + 2 * plywood_thickness,
			       internal_dimensions[2] +
			       2 * plywood_thickness + 2 * plywood_gap,
			       "Front baffle frame" );
	       }
	       rotate([90,0,0]){
		    abs_cutout_rectangle( woofer_center, abs_window_dimensions,
					  "ABS cutout");

	       }
	       rotate([90,0,0]) {
		    // Press-in-place #6 nuts (use #3 drill = 0.213 inches diameter)
		    for (location = pcb_mounting_holes) {
			 abs_cutout_round( [location[0], location[1]], 0.213/2);
		    }
	       }

	  }
     }
}

//**************************** Plywood *****************************//
if (show_plywood) {
     //bottom
     translate([0,0,plywood_thickness/2]) {
	  plywood(internal_dimensions[0] + 2 * plywood_thickness,
		  internal_dimensions[1]);	  
     }
     // Sides
     translate([plywood_thickness/2 + internal_dimensions[0] + plywood_thickness, 0,
		plywood_thickness + plywood_gap - plywood_antigap]){
	  rotate([0,-90,0]) {
	       plywood(internal_dimensions[2], internal_dimensions[1]);
	  }

     }
     translate([plywood_thickness/2, 0,
		plywood_thickness + plywood_gap - plywood_antigap]){
	  rotate([0,-90,0]) {
	       plywood(internal_dimensions[2], internal_dimensions[1]);
	  }

     }
     // Top
     translate([0, 0, plywood_thickness/2 + internal_dimensions[2] +
		plywood_thickness + 2 * plywood_gap - 2 * plywood_antigap]) {
	  plywood(internal_dimensions[0] + 2 * plywood_thickness,
		  internal_dimensions[1]);
     }

     // Back plywood
     translate([0, plywood_thickness/2 + internal_dimensions[1] + plywood_gap + abs_gap,
		0]) {
	  difference() {
	       translate([0,0 - plywood_antigap, 0]) {
		    rotate([90,0,0]) {
			 plywood(internal_dimensions[0] + 2 * plywood_thickness,
				 internal_dimensions[2] + 2 * plywood_thickness +
				 2 * plywood_gap);
		    }  
	       }

	       rotate([90,0,0]){
		    plywood_cutout_round([woofer_center[0],woofer_center[1]],
					 plywood_hole_diameter/2);
	       }
	       rotate([90,0,0]) {
		    // Tee nuts for #6 (use l1/64 drill = 0.1718 inches diameter)
		    for (location = al_mounting_holes) {
			 plywood_cutout_round( [location[0], location[1]], 0.1718/2);	 
		    }
	       }

	  }

     }
}

//********************** Rear connector panel **********************//

// Create a connector panel origin at the panel's lower left corner.
// We'll be able to use this to report x/y positions of holes in the
// panel.  Keep in mind that we'll need to use x and z coordinates to
// calculate these because of how the box is oriented.
al_origin = [
     (external_dimensions[0] + al_dimensions[0])/2,
     (external_dimensions[2] - al_dimensions[1])/2];
al_mounting_holes = [
     [al_origin[0] - 0.25, al_origin[1] + 0.25],
     [al_origin[0] - al_dimensions[0] + 0.25, al_origin[1] + 0.25],
     [al_origin[0] - al_dimensions[0] + 0.25,
      al_origin[1] + al_dimensions[1] - 0.25],
     [al_origin[0] - 0.25, al_origin[1] + al_dimensions[1] - 0.25]];
if (show_al) {
     translate([(external_dimensions[0] - al_dimensions[0])/2,
		al_thickness/2 + internal_dimensions[1] + abs_gap +
		plywood_thickness + al_gap,
		(external_dimensions[2] - al_dimensions[1])/2]) {
	  difference() {
	       rotate([90,0,0]) {
		    al_sheet(al_dimensions[0], al_dimensions[1], al_thickness);
	       }
	       rotate([90,0,0]) {
		    // 0.15 inches is a good clearance hole for #6
		    al_cutout_round([0.25, 0.25], 0.15/2);
		    al_cutout_round([al_dimensions[0] - 0.25, 0.25], 0.15/2);
		    al_cutout_round([al_dimensions[0] - 0.25,
					 al_dimensions[1] - 0.25], 0.15/2);
		    al_cutout_round([0.25, al_dimensions[1] - 0.25], 0.15/2);
	       }

	  }

     }    
}

// Mounting bolts for connector panel
if (show_bolts) {
     translate([0, al_thickness/2 + internal_dimensions[1] + abs_gap +
		plywood_thickness + al_gap + bolt_gap, 0]) {
	  for (location = al_mounting_holes) {
	       translate([location[0], 0, location[1]]) {
		    rotate([270,0,0]){
			 socket_cap_six(5/8);
		    }
	       }
	  }
     }
}

// Tee nuts for connector panel
if (show_bolts) {
     translate([0, internal_dimensions[1] + abs_gap - bolt_gap, 0]) {
	  for (location = al_mounting_holes) {
	       translate([location[0], 0, location[1]]) {
		    rotate([90,0,0]){
			 tee_nut_six();
		    }
	       }
	  }
     }
}

//*************************** Inner PCB ****************************//

translate([ internal_dimensions[0]/2 - inpcb_dimensions[0]/2 + plywood_thickness,
	    internal_dimensions[1]/2 - inpcb_dimensions[1]/2,
	    plywood_thickness + inpcb_spacer]) {
     pcb( [inpcb_dimensions[0],
	   inpcb_dimensions[1],
	   inpcb_thickness], "Inner PCB" );
}




