\section{Cabinet}

Figure \ref{fig:cabinet_plywood_iso} shows the cabinet.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.5]{pngs/cabinet_plywood_iso}
    \caption{Plywood cabinet assembly. \label{fig:cabinet_plywood_iso}}
  \end{center}
\end{figure}

\clearpage
Figure \ref{fig:cabinet_dims} shows the cabinet dimensions.  All
tolerances are $\pm$0.1 inches.  I prefer the large round hole to be
made with a hole saw, but I'll accept a jigsaw cut -- this hole will
ultimately be hidden by a connector plate.  The four mounting holes
for this plate are not called out.  I'll drill them when the plate is
finalized.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.3]{figs/cabinet_dims}
    \caption{Plywood cabinet dimensions. Cabinet is made from five
      pieces of 1/2-inch thick Baltic birch
      plywood.\label{fig:cabinet_dims}}
  \end{center}
\end{figure}

\clearpage
\subsection{Connector plate}

\subsubsection{With binding posts}

Figure \ref{fig:rearplate_bp_annotated} shows dimensions for the rear
connector plate with binding posts.  The binding posts are Keystone
7004 (red) and 7005 (black).  Front panel designer allows creating
these ``double D-Holes.''

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.8]{figs/rearplate/rearplate_bp_annotated}
    \caption{Rear connector plate with binding posts.  All dimensions
      are in inches.  The corner mounting holes are for \#6 socket-cap
      screws.  These can be installed with a $\frac{7}{64}$-inch
      balldriver.\label{fig:rearplate_bp_annotated}}
  \end{center}
\end{figure}

Front Panel Designer allows embedding pdf images as ``print
graphics.''  Create the pdf logo using the makefile in the marketing directory:
\begin{center}
  \begin{minipage}{10cm}
    \dirtree{%
      .1 projects.
      .2 eventual.
      .3 eventual.
      .4 marketing.
      .5 logo.
      .6 makefile.
    }
  \end{minipage}
\end{center}
...and the logo won't be the right size for the front panel.  Use the
Front Panel Designer software to scale the image before embedding.

\clearpage{}
\subsubsection{With USB}

Figure \ref{fig:rearplate_usb} shows dimensions for the rear connector
plate with USB and power connections.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.8]{figs/rearplate/rearplate_usb}
    \caption{Rear connector plate with USB and power connections.  All dimensions
      are in inches.  The corner mounting holes are for \#6 socket-cap
      screws.  These can be installed with a $\frac{7}{64}$-inch
      balldriver.\label{fig:rearplate_usb}}
  \end{center}
\end{figure}

\subsubsection{Gasket}

Use $\frac{1}{16}$-inch thick natural rubber for gaskets.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.7]{figs/rearplate/gasket}
    \caption{The connector plate gasket.  All dimensions are in
      inches.  The gasket is $\frac{1}{16}$-inch thick.\label{fig:gasket}}
  \end{center}
\end{figure}

\subsubsection{Installation}

The 6-32 tee nut inserts have a barrel diameter of 0.169 inches, and
require a $\frac{11}{64}$-inch drill for installation.  The closest wood drill
size at McMaster-Carr is $\frac{3}{16}$.

Position the rear plate as shown in figure
\ref{fig:rearplate_alignment}.  Mark the mounting hole positions with
a center punch.  Drill the holes with a $\frac{3}{16}$-inch brad drill.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.6]{figs/rearplate/rearplate_alignment}
    \caption{Aligning the connector plate.\label{fig:rearplate_alignment}}
  \end{center}
\end{figure}

Put a dab of silicone glue on each tee nut before pressing each into
the drilled holes.  Put $\frac{3}{8} \times \frac{9}{64}$ washers on
6-32 x 1-inch socket-cap screws before inserting them into the
mounting plate holes.  Position the mounting plate with the screws on
the cabinet and carefully thread the screws into the tee nuts.  Use a
$\frac{7}{64}$ hex key to tighten down on the screws -- installing the
tee nuts into the wood cabient.

After the tee nuts are installed, replace the 1-inch screws with 6-32
x $\frac{5}{8}$ screws.

\clearpage

\subsection{Plastic frame (airhole -- 23-12)}

Figure \ref{fig:frame} shows the plastic frame used to hold the front
baffle.  6-32 press-fit nuts are pressed into the 8 drilled holes.
These nuts are McMaster-Carr \textbf{94648A330} and are specified for
use with a \#3 drill (0.213-inch diameter).

Figure \ref{fig:broaching_nuts} shows the drill parameters required
for countersinking the broaching nuts.  This scheme would require 4mm
instead of 3mm Perspex.  But it would reduce the diameter of holes
needed in the wooden cabinet from $\frac{3}{8}$ inches to about
$\frac{1}{4}$ inch.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.6]{figs/frame/frame}
    \caption{Baffle mounting frame.  Material is 3mm thick Perspex (acrylic).\label{fig:frame}}
  \end{center}
\end{figure}

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.6]{figs/frame/broaching_nuts}
    \caption{Hole profile for countersunk broaching nuts.  This
      scheme would require 4mm Perspex.\label{fig:broaching_nuts}}
  \end{center}
\end{figure}

\subsubsection{Problems with press-fit nuts}

The press-fit nuts I bought from McMaster are PEM \mfgpart{p-kfs2-632}
``broaching nuts.'' Both PEM and McMaster recommend for these to be
pressed into 0.213-inch holes.  PEM recommends that these holes be at
least 0.22 inches from the edge of the drilled piece.  I observed both
these dimensions, and my piece cracked during installation.

Threads on these nuts are 0.231 inches in diameter.  The threads are
attached to a shaft about 0.210 inches in diameter.  A 0.213-inch hole
provides 0.018 inches of interference -- 9 mils on each side.  What is
the minimum interference I can get away with?

The fix, for acrylic anyway, is to heat the nuts before installation.
A soldering iron works well for this.  The nut will stay hot long
enough for you to press it in place.

\subsubsection{Installation}

The first revision of the plastic frame did not countersink its
broaching nuts.  You needed to drill into the wood cabinet with a
$\frac{3}{8}$ drill to create pockets for the nuts.  This is a
dangerous and sloppy procedure.  Moving to the countersinking scheme
illustrated in figure \ref{fig:broaching_nuts} would reduce the size
of these pockets.

\subsection{Baffle}

Figure \ref{fig:baffle_pcb_drawing} shows where holes should be
drilled in the PCB baffle.  Figure \ref{fig:pot_mounting} describes
mounting holes needed for the volume knob.

\begin{itemize}
\item SMTSO 6-32 surface-mount nuts require a 0.213 diameter mounting
  hole and 0.306 diameter annulus.
  \begin{itemize}
  \item Part number is SMTSO-632-2ET for 6-32 nuts
  \item S.W. Anderson will only sell reels of these -- 1500 per reel
    ($\sim$\$400).
  \end{itemize}
\end{itemize}

\begin{itemize}
\item SMTSO 4-40 surface-mount nuts require a 0.166 diameter mounting
  hole and 0.244 diameter annulus.
  \begin{itemize}
  \item Part number is SMTSO-440-2ET for 4-40 nuts
  \end{itemize}
\end{itemize}

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.6]{figs/baffle/baffle}
    \caption{Baffle PCB dimensions. \label{fig:baffle_pcb_drawing}}
  \end{center}
\end{figure}

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.2]{pngs/3852a_pot_mounting}
    \caption{Mounting holes needed for 3852A potentiometers from Bourns. \label{fig:pot_mounting}}
  \end{center}
\end{figure}

\subsubsection{Mounting screws}

Mounting screws should be as short as possible to avoid damaging the
cabinet.  Equation \ref{eq:baffle_screws} shows how the screw lengths
should be calculated.
\begin{equation}
  \label{eq:baffle_screws}
  \ell = (\mathrm{PCB\; thickness}) + (\mathrm{frame\; thickness})
\end{equation}

\begin{itemize}
\item For a $\frac{1}{16}$-inch PCB and 4mm frame, the proper size is
  0.22 inches.  $\frac{1}{4}$ inch screws are fine with \textbf{0-31} washers.
\end{itemize}


\clearpage{}
\subsubsection{Gasket (adina -- 23-11)}

The front baffle dimensions are shown in figure
\ref{fig:baffle_pcb_drawing}.  I want to pull the gasket back from the
edges by 0.01 inches at each edge.  Figure \ref{fig:baffle_gasket}
shows the gasket.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.4]{figs/baffle/adina}
    \caption{Gasket for front baffle. All dimensions are in inches.
      Material is general-purpose neoprene with a durometer of
      30A.\label{fig:baffle_gasket}}
  \end{center}
\end{figure}

\clearpage{}
\subsubsection{Inner PCB (afterglow -- 13-10)}

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.6]{figs/inpcb/inner_pcb_mounting}
    \caption{Inner PCB dimensions (inches) and mounting
      holes.\label{fig:afterglow_dimensions}}
  \end{center}
\end{figure}

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.6]{figs/inpcb/inner_pcb_connectors}
    \caption{Connectors on the inner PCB.\label{fig:afterglow_connectors}}
  \end{center}
\end{figure}