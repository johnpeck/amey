\section{Impedance measurements}

\subsection{Measuring impedance with the CGR-201}

The CGR-201 from Syscomp Design has a 50$\Omega$ output and two
inputs.  Figure \ref{fig:cgr-201_connections} shows how to connect the
output and inputs to measure voice coil impedances.  

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.6]{figs/impedance/cgr-201_connections}
    \caption{Connecting the CGR-201 to measure voice coil impedance.
      \label{fig:cgr-201_connections}}
  \end{center}
\end{figure}

The Circuitgear software measures
\begin{equation}
  \label{eq:raw_transfer}
  A_{cg} = \frac{v_b}{v_a}
\end{equation}
and records it as a complex number at each frequency step.  We can
relate the voice coil impedance $Z_{vc}$ to $A_{cg}$ with
\begin{equation}
  \label{eq:total_impedance}
  Z_{vc} + R_s = \frac{v_a}{i_{vc}} = R_s\frac{v_a}{v_b} = \frac{R_s}{A_{cg}}
\end{equation}
where $i_{vc}$ is current in the voice coil.  Solving this for
$Z_{vc}$ gives
\begin{equation}
  \label{eq:zvc_from_acg}
  Z_{vc} = R_s \left( \frac{1}{A_{cg}} - 1 \right).
\end{equation}

\subsection{Equivalent circuit parameters}

Small has introduced system parameters to describe moving-coil
loudspeakers\cite{small_1972}.  Figure \ref{fig:system_equivalent}
uses his notation for equivalent circuit elements.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.8]{figs/impedance/equivalent}
    \caption{Equivalent circuit for a dynamic loudspeaker in a
      closed box.
      \label{fig:system_equivalent}}
  \end{center}
\end{figure}

The impedance of the equivalent circuit is given by
\begin{equation}
  \label{eq:impedance}
  Z_{vc}(s) = R_e \left( \frac{s^2 + \omega_c \left( \frac{1}{Q_{mc}}
        + \frac{1}{Q_{ec}} \right) s + \omega_c^2}{s^2 +
      \frac{\omega_c}{Q_{mc}}s + \omega_c^2} \right)
\end{equation}
where I use the $c$ subscript to indicate parameters for a closed
box.  Figure \ref{fig:simulated_impedance} shows a calculated
impedance curve.

\begin{figure}[ht]
  \begin{center}
    \includegraphics[clip,scale=0.8]{scripts/simimp}
    \caption{Simulated impedance
      \label{fig:simulated_impedance}}
  \end{center}
\end{figure}

The electrical $Q_{ec}$ is formed by
\begin{equation}
  \label{eq:qec}
  Q_{ec} = \omega_c R_e C_{mec}
\end{equation}
where $C_{mec}$ represents the moving mass -- the driver mass plus
anything extra caused by mounting it in a closed box.  The mechanical
$Q_{mc}$ is formed by
\begin{equation}
  \label{eq:qmc}
  Q_{mc} = \omega_c R_{ec} C_{mec}
\end{equation}
where $R_{ec}$ represents mechanical resistance to driver motion.