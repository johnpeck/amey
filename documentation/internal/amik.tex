\section{Amik -- codebase for Amey}

\subsection{Arduino UNO fuses}

Logic high means that the fuse is unprogrammed.  Table
\ref{tab:arduino_uno_fuse_defaults} lists default fuse settings.

% See doctools/latex/lengths.tex for the list of column widths
\setlength{\namewidth}{3cm}
\setlength{\valuewidth}{3cm}
\setlength{\commentwidth}{8cm}

\begin{table}[ht]
  \begin{center}
    \begin{tabular}{|c|c|c|}
      \hline

      \cwksentry{\namewidth}{Fuse}
      &\cwksentry{\valuewidth}{Default}
      &\cwksentry{\commentwidth}{Comment}\\
      \hline \hline
      % End of column headings

      Low
      &\texttt{0xff}
      &\wksentry{\commentwidth}{%
        \begin{itemize}
        \item External crystal clock source
        \item No system clock divider
        \end{itemize}
      }\\
      \hline

      High
      &\texttt{0xde}
      &\wksentry{\commentwidth}{%
        \begin{itemize}
        \item Serial programming enabled
        \item EEPROM not saved during chip erase
        \item Execution jumps to boot loader address after reset.
        \end{itemize}
      }\\
      \hline

      Extended
      &\texttt{0x05}
      &\wksentry{\commentwidth}{%
        \begin{itemize}
        \item Undefined brown-out detector level
        \end{itemize}
      }\\
      \hline

    \end{tabular}

    \caption{Default fuse settings for the Arduino
      UNO.\label{tab:arduino_uno_fuse_defaults}}

  \end{center}
\end{table}

\rednote{If the fuse settings cause the part to expect an external
  crystal and none exists (or isn't installed correctly), it will
  never be able to recover.}

\subsection{Fuse settings for development}

We'll want to skip the bootloader during development -- writing flash
with a dedicated programmer.  Table \ref{tab:amik_dev_fuse_settings} lists
the values for this flow.  Remember that these are just the values
applied before any code is run -- programmed code can override these
settings at runtime.

\begin{table}[ht]
  \begin{center}
    \begin{tabular}{|c|c|c|}
      \hline

      \cwksentry{\namewidth}{Fuse}
      &\cwksentry{\valuewidth}{Default}
      &\cwksentry{\commentwidth}{Comment}\\
      \hline \hline
      % End of column headings

      Low
      &\texttt{0xe2}
      &\wksentry{\commentwidth}{%
        \begin{itemize}
        \item Calibrated internal 8MHz RC clock source.
        \item No clock frequency divider.
        \item $\sim$70ms start-up time for oscillator.
        \end{itemize}
      }\\
      \hline

      High
      &\texttt{0xd9}
      &\wksentry{\commentwidth}{%
        \begin{itemize}
        \item Serial programming enabled.
        \item EEPROM not saved during chip erase
        \item Execution jumps to address 0x0 after reset.
        \end{itemize}
      }\\
      \hline

      Extended
      &\texttt{0x07}
      &\wksentry{\commentwidth}{%
        \begin{itemize}
        \item Brown-out detector disabled.
        \end{itemize}
      }\\
      \hline

    \end{tabular}

    \caption{Fuse settings for Amik development.\label{tab:amik_dev_fuse_settings}}

  \end{center}
\end{table}

\clearpage
\subsection{Fuse settings for release}

The final product will not provide access for a hardware programmer,
so released code will need to support a bootloader.  Table
\ref{tab:amik_rel_fuse_settings} lists the required fuse settings.

\begin{table}[ht]
  \begin{center}
    \begin{tabular}{|c|c|c|}
      \hline

      \cwksentry{\namewidth}{Fuse}
      &\cwksentry{\valuewidth}{Default}
      &\cwksentry{\commentwidth}{Comment}\\
      \hline \hline
      % End of column headings

      Low
      &\texttt{0xe2}
      &\wksentry{\commentwidth}{%
        \begin{itemize}
        \item Calibrated internal 8MHz RC clock source.
        \item No clock frequency divider.
        \item $\sim$70ms start-up time for oscillator.
        \end{itemize}
      }\\
      \hline

      High
      &\texttt{0xdc}
      &\wksentry{\commentwidth}{%
        \begin{itemize}
          \item \texttt{BOOTRST} fuse programmed -- execution will jump
            to the bootloader after reset.
          \item Maximum bootloader size of 512 words = 1024 bytes.
          \item EEPROM not saved during chip erase.
          \item SPI programming enabled.  We need this to flash the bootloader.
        \end{itemize}
      }\\
      \hline

      Extended
      &\texttt{0x07}
      &\wksentry{\commentwidth}{%
        \begin{itemize}
        \item Brown-out detector disabled.
        \end{itemize}
      }\\
      \hline

    \end{tabular}

    \caption{Fuse settings for Amik releases with a bootloader.
      Remember that programmed fuses have a value of 0, and
      unprogrammed fuses have a value of 1.\label{tab:amik_rel_fuse_settings}}

  \end{center}
\end{table}


\subsection{Programming with conflicting USB drivers}

There's nothing you can do to prevent multiple USB drivers from being
installed on Windows.  Luckily, \textbf{libusb-win32} has a Filter
Wizard GUI that lets you tell windows to use libusb for programmer
communication.

\clearpage
\subsection{Bootloader}

I'm using the \href{https://github.com/mrpace2/kavr}{kavr} bootloader,
which allows uploading code through the USART at flexible baud rates.

\subsubsection{How big is it?}

The \texttt{avr-size} command shows:

% Note that I only use the commandchars option here to make emacs
% happy -- it gets confused about math mode if I don't escape the
% dollar sign, but I don't want the escape character printed.
\begin{Verbatim}[frame=single,commandchars=\\\{\}]
~/projects/eventual/amik/src/bootloader \$ avr-size kavr.hex
   text	   data	    bss	    dec	    hex	filename
      0	    536	      0	    536	    218	kavr.hex
\end{Verbatim}

\noindent ...where \texttt{dec} is the size of the bootloader in bytes
formatted as a decimal number.  The boot sizes in the AVR datasheet
are in words, which are two bytes large.  Notice that the size of the
bootloader is just above 512 bytes.  We can't use the smallest boot
size, and we have to use the next larger 1024-byte size.  This means
\texttt{BOOTSZ1} = 1 (unprogrammed) and \texttt{BOOTSZ0} = 0
(programmed) in the high fuse settings.

\subsubsection{What is the reset address?}

If we know the \texttt{BOOTSZx} fuse settings, we can look up the boot
reset address in the AVR datasheet.  This is a word address, and thus
not the address to use in the \textbf{kavr} makefile.  The byte
address for the makefile is twice the word address.  So, if the
datasheet calls for an address of \texttt{0x3e00}, we'll set
\texttt{bootaddr} to \texttt{0x7c00}.

\subsubsection{Sending new code}

Once the proper fuses and the bootloader have been programmed, you can
use a program like Tera Term to write new application code.  Make a
normal connection and make sure flow control is set to Xon/Xoff. Reset
the board, and use \menuitem{file} $\rightarrow$ \menuitem{send file}
to send the new hex file.  You have to be quick -- the bootloader will
only wait a short (programmable) time for new data to arrive after a reset.
