
function mm_to_in (mm) = mm/25.4;

module socket_cap_six( length ) {
     // Create a #6 socket cap screw

     // Head height
     head_height = 0.138;

     // Head diameter
     head_diameter = 0.226;

     // Bolt diameter
     bolt_diameter = 0.138;

     // Head
     color( "Silver") {
	  translate([0, 0, 0]) {
	       cylinder( h = head_height, r = head_diameter/2, $fn=100);
	  }
     }
     // Threads
     color( "Gray") {
	  translate([0, 0, 0 - length]) {
	       cylinder( h = length, r = bolt_diameter/2, $fn=100);
	  }
     }
     echo ( str("6-32 x ", length, " socket cap screw"));
}

module socket_cap_four( length ) {
     // Create a #4 socket cap screw

     // Head height
     head_height = 0.112;

     // Head diameter
     head_diameter = 0.161;

     // Bolt diameter
     bolt_diameter = 0.112;

     // Head
     color( "Silver") {
	  translate([0, 0, 0]) {
	       cylinder( h = head_height, r = head_diameter/2, $fn=100);
	  }
     }
     // Threads
     color( "Gray") {
	  translate([0, 0, 0 - length]) {
	       cylinder( h = length, r = bolt_diameter/2, $fn=100);
	  }
     }
     echo ( str("4-40 x ", length, " socket cap screw"));
}

module tee_nut_six() {
     // Create a #6 tee nut

     flange_height = 0.031;
     flange_diameter = 0.5;

     shaft_height = 0.25;
     shaft_diameter = 0.169;

     thread_diameter = 0.13;

     difference() {
	  union() {
	       // Shaft
	       color( "SlateGray") {
		    translate([0, 0, 0 - shaft_height]) {
			 cylinder( h = shaft_height, r = shaft_diameter/2, $fn=100);
		    }
	       }
	       // Flange
	       color( "LightGray" ) {
		    translate([0, 0, 0]) {
			 cylinder( h = flange_height, r = flange_diameter/2, $fn=100);
		    }
	       }
	  }
	  translate([0, 0, -1.5 * shaft_height]) {
	       cylinder( h = 2 * shaft_height, r = thread_diameter/2, $fn=100);
	  }

     }
     echo ( str("6-32 tee nut is McMaster 90973A035"));

}

module pem_six() {
     // Create a PEM nut for #6

// Head height
     head_height = 0.07;

     // Head diameter
     head_diameter = 0.28;

     // Sink height
     sink_height = 0.06;

     // Sink diameter
     sink_diameter = 0.213;

     difference() {
	  union() {
	       // Head
	       color( "SlateGray") {
		    translate([0, 0, 0]) {
			 cylinder( h = head_height, r = head_diameter/2, $fn=100);
		    }
	       }
	       // Sunk part
	       color( "DarkSlateGray") {
		    translate([0, 0, 0 - sink_height]) {
			 cylinder( h = sink_height, r = sink_diameter/2, $fn=100);
		    }
	       }
	  }
	  cylinder( h = 2 * head_height + 2 * sink_height,
		    r = 0.146/2,
		    center = true,
		    $fn=100);
     }
     echo ( str("6-32 press-fit nut is McMaster 94648A330"));
}

module pem_four() {
     // Create a PEM nut for #4

// Head height
     head_height = 0.07;

     // Head diameter
     head_diameter = 0.22;

     // Sink height
     sink_height = 0.06;

     // Sink diameter
     sink_diameter = 0.166;

     difference() {
	  union() {
	       // Head
	       color( "SlateGray") {
		    translate([0, 0, 0]) {
			 cylinder( h = head_height, r = head_diameter/2, $fn=100);
		    }
	       }
	       // Sunk part
	       color( "DarkSlateGray") {
		    translate([0, 0, 0 - sink_height]) {
			 cylinder( h = sink_height, r = sink_diameter/2, $fn=100);
		    }
	       }
	  }
	  cylinder( h = 2 * head_height + 2 * sink_height,
		    r = 0.112/2,
		    center = true,
		    $fn=100);
     }
     echo ( str("4-32 press-fit nut is McMaster 94648A320"));
}

// socket_cap_six(1);
// pem_six();
// pem_four();
tee_nut_six();

